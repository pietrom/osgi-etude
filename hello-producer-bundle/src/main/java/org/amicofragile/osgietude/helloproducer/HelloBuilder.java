package org.amicofragile.osgietude.helloproducer;

public class HelloBuilder {

	public String sayHello(String to) {
		return String.format("Hello, %s!", to.toUpperCase());
	}
	
}
