package org.amicofragile.osgietude.helloproducer;

import org.junit.Assert;
import org.junit.Test;

public class HelloBuilderTest {
	@Test
	public void sayHelloToTarget() {
		Assert.assertEquals("Hello, WORLD!", new HelloBuilder().sayHello("World"));
	}
}
