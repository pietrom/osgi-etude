package org.amicofragile.osgietude.hello;

import org.amicofragile.osgietude.hellopublisher.HelloPublisher;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

public class HelloActivator implements BundleActivator {
	private static final String TARGET = "OSGI newbie";
	private HelloPublisher producer;
	@Override
	public void start(BundleContext context) throws Exception {
		ServiceReference serviceReference = context.getServiceReference(HelloPublisher.class.getName());
		producer = (HelloPublisher) context.getService(serviceReference);
		String message = producer.buildHello(TARGET);
		System.out.println("++ Start 'hello' bundle: " + message);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		String message = producer.buildHello(TARGET);
		System.out.println("-- Stop 'hello' bundle: " + message);
	}
}
