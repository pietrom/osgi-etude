package org.amicofragile.osgietude.hellopublisher.simple;

import org.amicofragile.osgietude.helloproducer.HelloBuilder;
import org.amicofragile.osgietude.hellopublisher.HelloPublisher;

public class Publisher implements HelloPublisher {
	private final HelloBuilder builder = new HelloBuilder();

	@Override
	public String buildHello(String target) {
		return builder.sayHello(target);
	}
}
