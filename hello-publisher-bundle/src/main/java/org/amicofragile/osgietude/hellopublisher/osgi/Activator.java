package org.amicofragile.osgietude.hellopublisher.osgi;

import org.amicofragile.osgietude.hellopublisher.HelloPublisher;
import org.amicofragile.osgietude.hellopublisher.simple.Publisher;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

public class Activator implements BundleActivator {
	private ServiceRegistration registration;

	@Override
	public void start(BundleContext context) throws Exception {
		HelloPublisher publisher = new Publisher();
		this.registration = context.registerService(HelloPublisher.class.getName(), publisher, null);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		this.registration.unregister();
	}
}
