package org.amicofragile.osgietude.hellopublisher;

public interface HelloPublisher {
	public abstract String buildHello(String target);
}
